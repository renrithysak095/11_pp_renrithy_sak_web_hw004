/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    fontFamily:{
        'labrada' : 'Mochiy Pop One, sans-serif;',
        'labrada-v1' : 'Labrada, serif;',
        'labrada-3': 'Labrada, serif;',
        'Tajawal': 'Tajawal, sans-serif;',
        'fas' : 'Faustina, serif;',
        'freeB': 'Bree Serif, serif;',
    },
    extend: {
      width :{
        '1' : '550px',
        'div-1' : '80%',
        'card-w': '320px',
        'btn-w': '120px',
        'w-lc' : '100px',
        'w-rc' : '380px',
        'middle': '100%',
        'w-100vh': '100vh'

      },
      height :{
        'h1'  : '265px',
        'h-lc': '100vh',
      },
      colors :{
        'blue' : '#61DAFB',
        'card-bg' : '#2D4250',
        'card-bg-v1': '#00243A',
        'white-low' : '#cbd5e1',
        'blue-old' : '#3577F5',
        'afterHoverD': '#282F3D',
        'beforeHoverD' : '#353B47',
        'lc-colors': '#F7F9FB',
        'btn-rc': '#E3D9A6',
        '+23': '#FDEDD5',
        'bg-l': '#E08B93',
        'color-text': '#B30543',
        'bg-gray-color': '#DBDDDC',
        'text-title': '#0F7FC4',
        'bg-middle': '#EAEAEA',
        
        
      }
    },
    backgroundImage: {
      'r-img': "url('../public/Images/Images/static5.jpg')",
      'raamin': "url('../public/Images/Images/raamin.jpg')",
      'christina': "url('../public/Images/Images/christina.jpg')",
      'lachlan': "url('../public/Images/Images/lachlan.jpg')",
      'nonamesontheway': "url('../public/Images/Images/nonamesontheway.jpg')",
      'noti' : "url('../public/Images/Images/notification.png')",
      'comment' : "url('../public/Images/Images/comment.png')",
    },
    fontSize : {
      '1' : '23px',
      '2' : '16px',
      '3' : '70px',
      '4' : '20px',
      '5' : '14px',
      '6' : '27px',
      '7' : '50px',
      '8': '28px',
      'plus+': '30px',
      'plus23': '17px',
    },
  },
  plugins: [
    require('flowbite/plugin'),
    require("daisyui")
  ],
}