import React, { useState } from "react";
import AddNewTrip from "./AddNewTrip";
import SampleCard from "./SampleCard";
export default function CardComponent() {
  const [place, setPlace] = useState([
    {
      id: 1,
      title: "Koh Kong Krav",
      description:
        "Koh Kong Krav Beach is in the 5th place out of 13 beaches in the Koh Kong region The beach is located in a natural place, among the mountains. It is partially covered 	   by trees which give natural shade. It is a spacious coastline with crystal turquoise water and white fine sand, so you don't need special shoes.",
      status: "Beach",
      peopleGoing: "1537",
    },
    {
      id: 2,
      title: "Everest",
      description:
        "Mount Everest is a peak in the Himalaya mountain range. It is located between Nepal and Tibet, an autonomous region of China. At 8,849 meters (29,032 feet), it is considered the tallest point on Earth. In the nineteenth century, the mountain was named after George Everest, a former Surveyor General of India. The Tibetan name is Chomolungma, which means “Mother Goddess of the World.” The Nepali name is Sagarmatha, which has various meanings.",
      status: "Mountain",
      peopleGoing: "81000",
    },
    {
      id: 3,
      title: "Kirirom",
      description:
        "Kirirom National Park, a high altitude plateau, is known for its unique high elevation pine forest, which forms the headwaters for numerous streams feeding Kampong 	   	   Speu Town.",
      status: "Forest",
      peopleGoing: "2500",
    },
  ]);

  //button 3 option
  const handleChange = (items) => {
    for (let i = 0; i < place.length; i++) {
      if (place[i].id === items.id) {
        items.status =
          items.status === "Beach"
            ? "Mountain"
            : items.status === "Mountain"
            ? "Forest"
            : items.status === "Forest"
            ? "Beach"
            : "Beach";
        const newPlace = [...place];
        newPlace[i] = items;
        setPlace(newPlace);
        break;
      }
    }
  };
  //Details
  const [details, setDetail] = useState({})
  //Button Show Details
  const handleDetails = (items) => {
      setDetail(items)
  };

  //Input
  const [newInput, setnewInput] = useState({});
  const [myNull, setMynull] = useState({
    title: '',
    description: '',
    peopleGoing: '',
    status: '',
})
  const handleInput = (e) => {
      const {name,value} = e.target;
      
      if(e.target.value == '' || undefined){
        setnewInput({...myNull, [name] : value});
      }
      setnewInput({...newInput, [name] : value});
    };

  //Submit
    const handleSubmit = (e) => {
      e.preventDefault();
      setPlace([...place, {...newInput, id: place.length +1}])
      document.getElementById("form").reset();
      setnewInput([])
    }
  //HandlNull
  const handleNull = () => {
    document.getElementById("form").value.reset();
  }
    
  return (
    <div className="w-middle p-6 ">
      <div>
        <div className="flex flex-row justify-between items-center">
          <p className="text-7 text-card-bg-v1 font-fas">Good Evening Team!</p>
          <AddNewTrip handleInput = {handleInput}
          handleNull = {handleNull}
           handleSubmit = {handleSubmit}/>
        </div>

        <div className="justify-evenly grid grid-cols-3 gap-6">
          {place.map((items) => (
              <SampleCard items = {items} details = {details}
               handleChange = {handleChange} handleDetails= {handleDetails} />
          ))}
        </div>
      </div>
    </div>
  );
}
