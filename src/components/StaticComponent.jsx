import React from 'react'

export default function StaticComponent() {
  return (
    <div>
        <div className='h-h-lc w-w-rc bg-r-img bg-cover relative flex flex-row'>

              {/* Three icons */}
              <div className='w-auto mt-8 justify-center items-center absolute right-0'>
                <div className='w-auto flex flex-row mr-8'>
                    <div className='w-6 h-6 mr-4 bg-noti bg-cover'></div>
                    <div className='w-6 h-6 mr-4 bg-comment bg-cover'></div>
                    <div className='w-6 h-6 mr-4 bg-lachlan rounded-full bg-cover'></div>
                </div>
              </div>

              {/* Button */}
              <div className='absolute right-0 mt-16 mr-8'>
                  <button type="button" className=" text-white bg-btn-rc hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-5 px-5 py-1.5 mr-2 mb-2">My Amazing Trip</button>
              </div>

              {/* Text */}
              <div className='absolute mt-36 p-6'>
                  <p className='text-justify text-6 font-fas text-white'>
                    I like laying Down on the sand and looking at the moon.
                  </p>
              </div>

              {/* Text-1 */}
              <div className='absolute mt-80 p-6'>
                  <p className='text-justify text-4 font-fas text-white'>
                    27 People going to this trip
                  </p>
                  {/* Images */}
                  <div className='w-auto flex flex-row mt-4'>
                      <div className='w-10 h-10 mr-4 bg-lachlan border border-white bg-cover rounded-full'></div>
                      <div className='w-10 h-10 mr-4 bg-raamin border border-white bg-cover rounded-full'></div>
                      <div className='w-10 h-10 mr-4 bg-nonamesontheway border border-white bg-cover rounded-full'></div>
                      <div className='w-10 h-10 mr-4 bg-christina border border-white bg-cover rounded-full'></div>
                      <div className='w-10 h-10 mr-4 bg-lachlan border border-white bg-cover rounded-full'></div>
                      <div className='w-10 h-10 mb-2 flex justify-center items-center bg-+23 bg-cover rounded-full text-white text-plus23 font-bold border border-dashed border-orange-600'>23+</div>
                </div>
              </div>

        </div>
    </div>
  )
}
