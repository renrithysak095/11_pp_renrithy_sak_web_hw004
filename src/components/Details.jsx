import React from 'react'

export default function Details({handleDetails,items,details}) {
  return (
    <div>
        <label onClick={() => {
                    handleDetails(items)
                  }} htmlFor="my-modal-3" className="font-fas btn">
                    Read Details
                  </label>
                  <input
                    type="checkbox"
                    id="my-modal-3"
                    className="modal-toggle"
                  />
                  <div className="modal">
                    <div className="modal-box relative">
                      <label
                        htmlFor="my-modal-3"
                        className="btn btn-sm btn-circle absolute right-2 top-2"
                      >
                        ✕
                      </label>
                      <h1 id="h3" className="font-freeB text-text-title text-8 font-bold">
                        {details.title == undefined? "<Null>": details.title}
                      </h1>
                      <p id="p" className="font-freeB py-4 text-justify">
                        {details.description == undefined? "<Null>": details.description}
                      </p>
                      <p  className="font-freeB py-4 text-justify">
                        Around <span id="p1" className="font-bold font-freeB">{items.peopleGoing == undefined? "<?>": details.peopleGoing}</span> people going there
                      </p>
                    </div>
                  </div>
    </div>
  )
}
