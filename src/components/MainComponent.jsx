import React from 'react'
import CardComponent from './CardComponent'
import LeftSide from './LeftSide'
import StaticComponent from './StaticComponent'

export default function MainComponent() {
  return (
    <div className='flex flex-row justify-between'>
      <LeftSide/>
      <CardComponent/>
      <StaticComponent/>
    </div>
  )
}
