import React from 'react'

export default function AddNewTrip({handleInput,handleSubmit,handleNull}) {
  return (
    <div>
      <form onSubmit={handleNull} id="form">
          <label htmlFor="my-modal-1" className="btn font-fas">Add New Trip</label>
              <input type="checkbox" id="my-modal-1" className="modal-toggle" />
              <div className="modal">
                <div className="modal-box relative">
                  <label htmlFor="my-modal-1" className="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
                  <h1 className="text-lg font-bold">Title</h1>
                  <input
                  id="title"
                  onChange={handleInput}
                  name = "title"
                  type="text" placeholder="Title" className="mt-2 input input-bordered input-info w-middle" />
                  <h3 className="mt-2 text-lg font-bold">Description</h3>
                  <input
                  id="description"
                  name = "description"
                  onChange={handleInput}
                  type="text" placeholder="Description" className="mt-2 input input-bordered input-info w-middle" />
                  <h3 className="mt-2 text-lg font-bold">People Going</h3>
                  <input 
                  name ="peopleGoing"
                  id="peopleGoing"
                  onChange={handleInput}
                  type="text" placeholder="People Going" className="mt-2 input input-bordered input-info w-middle" />
                  <h3 className="mt-2 text-lg font-bold">Types of Adventure</h3>
                  <select 
                  onChange={handleInput}
                  name="status"
                  className="mt-2 select select-info w-middle">
                      <option disabled selected>----Choose any Option-----</option>
                      <option>Beach</option>
                      <option>Forest</option>
                      <option>Mountain</option>
                    </select>
                   <label onClick={handleSubmit}  htmlFor="my-modal-1" className="mt-4 btn w-32" >Submit</label>
                </div>
                
              </div>
          </form>
    </div>
  )
}
