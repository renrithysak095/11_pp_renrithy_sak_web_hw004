import React from 'react'
export default function LeftSide() {
  return (
    <div>
          <div className='bg-bg-gray-color shadow-[7px_0px_15px_-5px_rgb(0,0,0,0.5)] h-h-lc w-w-lc flex flex-col items-center justify-evenly'>

              <div className='w-6'>
                <img src="/Images/Images/category_icon.png" alt="" />
              </div>

              <div className='w-6'>
                <img className='mb-4' src="/Images/Images/cube.png" alt="" />

                <div className='relative'>
                  <div className='w-2 h-2 bg-red-600 rounded-full absolute bottom-5 -right-0 '></div>
                  <img className='mb-4' src="/Images/Images/list.png" alt="" />
                </div>

                <div className='relative'>
                  <div className='w-2 h-2 bg-red-600 rounded-full absolute bottom-4 -right-0 '></div>
                  <img className='mb-4' src="/Images/Images/messenger.png" alt="" />
                </div>

                <img src="/Images/Images/list.png" alt="" />
              </div>

              <div className='w-6'> 
                <img className='mb-4' src="/Images/Images/success.png" alt="" />
                <img className='mb-4' src="/Images/Images/security.png" alt="" />
                <img className='mb-4' src="/Images/Images/users.png" alt="" />
              </div>

              <div className='w-auto'>
                <div className='w-8 h-8 mb-2 bg-lachlan  bg-cover rounded-full'></div>
                <div className='w-8 h-8 mb-2 bg-raamin  bg-cover rounded-full'></div>
                <div className='w-8 h-8 mb-2 bg-nonamesontheway  bg-cover rounded-full'></div>
                <div className='w-8 h-8 mb-2 flex justify-center items-center bg-black bg-cover rounded-full text-white text-plus+ font-bold'>+</div>
              </div>

          </div>  
    </div>
  )
}
