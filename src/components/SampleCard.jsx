import React from 'react'
import Details from './Details';

export default function SampleCard({items,details,handleChange,handleDetails}) {
  return (
    <div>
      <div key={items.id}
              className="shadow-[18px_18px_15px_-10px_rgb(0,0,0,0.5)] mt-4 w-card-w bg-card-bg-v1 border border-gray-200 rounded-2xl"
            >
              <div className="p-5">
                <a href="#">
                  <p className="-mt-2 font-labrada-3 mb-2 text-1 font-bold tracking-tight text-white">
                    {items.title == undefined ? "Null": items.title}
                  </p>
                </a>
                <p
                  id="truncate"
                  className="mb-3 font-freeB text-2 leading-6 text-justify text-white-low dark:text-gray-400"
                >
                  {items.description == undefined ? "<Null>": items.description}
                </p>
                <p className="font-freeB mb-1 text-white">People Going</p>
                <p className="mb-3 -mt-2 text-1 text-bold font-freeB text-white dark:text-gray-400">
                {items.peopleGoing == undefined? "<?>": items.peopleGoing}
                </p>
                <div className="flex flex-row justify-between">
                  <button
                    href="#"
                    style={{
                      backgroundColor:
                        items.status === "Beach"
                          ? "#3376F4"
                          : items.status === "Mountain"
                          ? "#9199A7"
                          : items.status === "Forest"
                          ? "#169941"
                          : "#3376F4",
                    }}
                    onClick={() => {
                      handleChange(items);
                    }}
                    className="inline-flex text-center justify-center w-btn-w items-center font-fas font-bold text-2 text-white bg-blue-old rounded-lg hover:bg-blue-800"
                  >
                    {items.status == undefined? "<Where?>": items.status}
                  </button>
                  <Details handleDetails = {handleDetails} items={items}
                        details = {details}
                  />
                </div>
              </div>
            </div>
    </div>
  )
}
