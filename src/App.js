import "./App.css";
import "flowbite";
import MainComponent from "./components/MainComponent";

function App() {
  
  return (
    <div>
      <MainComponent/>
    </div>
  );
}

export default App;
